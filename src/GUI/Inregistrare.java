package GUI;

import Code.Client;
import Code.Eroare;
import Code.Utilizator;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.lang.String;

public class Inregistrare{
    private JPanel Containter;
    private JPanel Left;
    private JPanel Right;
    private JPanel Bottom;
    private JLabel Surname;
    private JLabel Firstname;
    private JLabel Email;
    private JLabel Password;
    private JTextField SurnameInput;
    private JTextField FirstNameInput;
    private JTextField EmailInput;
    private JPasswordField PasswordInput;
    private JButton RegisterButton;
    private JButton CancelButton;
    private JLabel Confirmare;
    private JPasswordField ConfirmInput;

    public Inregistrare(){
        JFrame frame = new JFrame("Inregistrare");
        frame.setContentPane(Containter);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
        RegisterButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String nume = SurnameInput.getText();
                String prenume = FirstNameInput.getText();
                String email = EmailInput.getText();
                String parola = String.valueOf(PasswordInput.getPassword());
                Utilizator u = new Utilizator(nume,prenume,email,parola,parola);
                if(u.getExistaEroare()){
                        new Eroare(u.getMesajDeEroare());
                }
                else{
                    Client client = new Client();
                    String query = new String();
                    query = "INSERT INTO users (nume,prenume,email,parola,isAdmin) VALUES ('" + nume + "','" + prenume + "','" + email + "','" + parola + "','" + "0');";
                    try {
                        client.connectServer(2,query);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    frame.dispose();
                    ///Exit app
                    ///System.exit(0);
                }
            }
        });
        CancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
