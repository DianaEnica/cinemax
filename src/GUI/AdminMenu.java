package GUI;

import CRUD.*;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import Code.Client;

public class AdminMenu {
    private JPanel container;
    private JButton adaugareSala;
    private JButton statusBilet;
    private JButton modificaSala;
    private JButton modificareBilet;
    private JButton achizitionareBilet;
    private JButton stergeBilet;
    private JButton TOTALBILETEVANDUTEINTREButton;
    private JButton TOTALBILETEDINTROButton;

    public AdminMenu() {
        JFrame frame = new JFrame("AdminMenu");
        frame.setContentPane(container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
        adaugareSala.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AdaugaSala add = new AdaugaSala();
            }
        });
        modificaSala.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ModificaSala modify = new ModificaSala();
            }
        });
        achizitionareBilet.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AdaugaBilet add = new AdaugaBilet();
            }
        });
        statusBilet.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Client cl = new Client();
                int tag = 6;
                String query;
                query = "SELECT * FROM tickets;";
                try {
                    cl.connectServer(tag,query);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        TOTALBILETEVANDUTEINTREButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AfisareBileteIntervalOrar view = new AfisareBileteIntervalOrar();
            }
        });
        TOTALBILETEDINTROButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                AfisareBileteSala view = new AfisareBileteSala();
            }
        });

        modificareBilet.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ModificaBilet modify = new ModificaBilet();
            }
        });
        stergeBilet.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                StergeBilet delete = new StergeBilet();
            }
        });
    }
}
