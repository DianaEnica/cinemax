package Code;

public class SalaCinema {
    private int ID;
    private String numeSala;
    private Integer numarLocuri;
    private int managerSala;
    private int oraDeschidere;
    private int oraInchidere;

    public SalaCinema(String nume, Integer numar, int manager, int open, int close) {
        this.numeSala = nume;
        this.numarLocuri = numar;
        this.managerSala = manager;
        this.oraDeschidere = open;
        this.oraInchidere = close;
    }

    @Override
    public String toString() {
        return  "'" + numeSala + "'," +
                numarLocuri + "," +
                oraDeschidere + "," +
                oraInchidere + "," +
                managerSala;
    }
}
