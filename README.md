Cinemax
=======

###Java Application
  
  * Login system with user and password
  * Adding and updating informations about cinema rooms
  * CRUD operations on Tickets (create, read, update, delete)
  * View all the tickets selled in a cinema room 
  * view all the tickets selled between to dates
       
  This application is based on a Client - Server communication using Sockets.        
  This communication is used to send commands from GUI (using a code method for every type of query used in the database) to Client, decoding, resend it to the Server.      
  The Server, which is connected to DataBase, get the answer, send it to Client and finally the answer is back to user.     
         
  Database is a relational type having three tables: Seats, Cinema Rooms and Tickets.      
         
  Application made in IntelIJ IDEA for a School Project.            
                
                
  � Enica Diana - Maria and Potec Tiberiu - Mihail 2018